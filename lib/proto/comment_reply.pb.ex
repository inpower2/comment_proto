defmodule Commentapi.UpdateReplyRequest do
  @moduledoc false
  use Protobuf, protoc_gen_elixir_version: "0.11.0", syntax: :proto3

  field :reply, 1, type: :string
  field :isdeletedbyadmin, 2, type: :bool
  field :userid, 3, type: :string
  field :id, 4, type: :string
  field :postid, 5, type: :string
  field :status, 6, type: :int32
  field :count, 7, type: :int32
  field :userlikes, 8, type: :int32
  field :commentid, 9, type: :string
  field :media_url, 10, type: :string, json_name: "mediaUrl"
end

defmodule Commentapi.UpdateReplyResponse do
  @moduledoc false
  use Protobuf, protoc_gen_elixir_version: "0.11.0", syntax: :proto3

  field :status, 1, type: :bool
end

defmodule Commentapi.DeleteReplyResponse do
  @moduledoc false
  use Protobuf, protoc_gen_elixir_version: "0.11.0", syntax: :proto3

  field :status, 1, type: :bool
end

defmodule Commentapi.DeleteReplyRequest do
  @moduledoc false
  use Protobuf, protoc_gen_elixir_version: "0.11.0", syntax: :proto3

  field :id, 1, type: :string
end

defmodule Commentapi.CreateReplyRequest do
  @moduledoc false
  use Protobuf, protoc_gen_elixir_version: "0.11.0", syntax: :proto3

  field :reply, 1, type: :string
  field :isdeletedbyadmin, 2, type: :bool
  field :userid, 3, type: :string
  field :postid, 4, type: :string
  field :status, 5, type: :int32
  field :count, 6, type: :int32
  field :userlikes, 7, type: :int32
  field :commentid, 8, type: :string
  field :media_url, 9, type: :string, json_name: "mediaUrl"
end

defmodule Commentapi.CreateReplyResponse do
  @moduledoc false
  use Protobuf, protoc_gen_elixir_version: "0.11.0", syntax: :proto3

  field :reply, 1, type: :string
  field :isdeletedbyadmin, 2, type: :bool
  field :userid, 3, type: :string
  field :postid, 4, type: :string
  field :status, 5, type: :int32
  field :count, 6, type: :int32
  field :userlikes, 7, type: :int32
  field :commentid, 8, type: :string
  field :media_url, 9, type: :string, json_name: "mediaUrl"
end

defmodule Commentapi.GetReplyRequest do
  @moduledoc false
  use Protobuf, protoc_gen_elixir_version: "0.11.0", syntax: :proto3

  field :commentid, 1, type: :string
end

defmodule Commentapi.GetReplyResponse do
  @moduledoc false
  use Protobuf, protoc_gen_elixir_version: "0.11.0", syntax: :proto3

  field :replies, 1, repeated: true, type: Commentapi.Replies
end

defmodule Commentapi.Replies do
  @moduledoc false
  use Protobuf, protoc_gen_elixir_version: "0.11.0", syntax: :proto3

  field :reply, 1, type: :string
  field :isdeletedbyadmin, 2, type: :bool
  field :userid, 3, type: :string
  field :id, 4, type: :string
  field :postid, 5, type: :string
  field :status, 6, type: :int32
  field :count, 7, type: :int32
  field :userlikes, 8, type: :int32
  field :commentid, 9, type: :string
  field :media_url, 10, type: Commentapi.Media, json_name: "mediaUrl"
  field :date, 11, type: :string
end

defmodule Commentapi.UpdateCommentRequest do
  @moduledoc false
  use Protobuf, protoc_gen_elixir_version: "0.11.0", syntax: :proto3

  field :comment, 1, type: :string
  field :isdeletedbyadmin, 2, type: :bool
  field :userid, 3, type: :string
  field :postid, 4, type: :string
  field :status, 5, type: :int32
  field :count, 6, type: :int32
  field :userlikes, 7, type: :int32
  field :id, 8, type: :string
  field :media_url, 9, type: :string, json_name: "mediaUrl"
end

defmodule Commentapi.UpdateCommentResponse do
  @moduledoc false
  use Protobuf, protoc_gen_elixir_version: "0.11.0", syntax: :proto3

  field :status, 1, type: :bool
end

defmodule Commentapi.DeleteCommentResponse do
  @moduledoc false
  use Protobuf, protoc_gen_elixir_version: "0.11.0", syntax: :proto3

  field :status, 1, type: :bool
end

defmodule Commentapi.DeleteCommentRequest do
  @moduledoc false
  use Protobuf, protoc_gen_elixir_version: "0.11.0", syntax: :proto3

  field :id, 1, type: :string
end

defmodule Commentapi.CreateCommentRequest do
  @moduledoc false
  use Protobuf, protoc_gen_elixir_version: "0.11.0", syntax: :proto3

  field :comment, 1, type: :string
  field :isdeletedbyadmin, 2, type: :bool
  field :userid, 3, type: :string
  field :postid, 4, type: :string
  field :status, 5, type: :int32
  field :userlikes, 7, type: :int32
  field :id, 8, type: :string
  field :media_url, 9, type: :string, json_name: "mediaUrl"
end

defmodule Commentapi.CreateCommentResponse do
  @moduledoc false
  use Protobuf, protoc_gen_elixir_version: "0.11.0", syntax: :proto3

  field :comment, 1, type: :string
  field :isdeletedbyadmin, 2, type: :bool
  field :userid, 3, type: :string
  field :postid, 4, type: :string
  field :status, 5, type: :int32
  field :count, 6, type: :int32
  field :userlikes, 7, type: :int32
  field :id, 8, type: :string
  field :media_url, 9, type: :string, json_name: "mediaUrl"
end

defmodule Commentapi.GetCommentRequest do
  @moduledoc false
  use Protobuf, protoc_gen_elixir_version: "0.11.0", syntax: :proto3

  field :postid, 1, type: :string
end

defmodule Commentapi.GetCommentResponse do
  @moduledoc false
  use Protobuf, protoc_gen_elixir_version: "0.11.0", syntax: :proto3

  field :comments, 1, repeated: true, type: Commentapi.Comment
end

defmodule Commentapi.Comment do
  @moduledoc false
  use Protobuf, protoc_gen_elixir_version: "0.11.0", syntax: :proto3

  field :comments, 1, type: :string
  field :isdeletedbyadmin, 2, type: :bool
  field :userid, 3, type: :string
  field :postid, 4, type: :string
  field :status, 5, type: :int32
  field :count, 6, type: :int32
  field :userlikes, 7, type: :int32
  field :id, 8, type: :string
  field :medias, 10, type: Commentapi.Media
  field :date, 11, type: :string
end

defmodule Commentapi.Media do
  @moduledoc false
  use Protobuf, protoc_gen_elixir_version: "0.11.0", syntax: :proto3

  field :media_url, 1, type: :string, json_name: "mediaUrl"
end

defmodule Commentapi.CommentService.Service do
  @moduledoc false
  use GRPC.Service, name: "commentapi.CommentService", protoc_gen_elixir_version: "0.11.0"

  rpc :CreateComment, Commentapi.CreateCommentRequest, Commentapi.CreateCommentResponse

  rpc :GetComment, Commentapi.GetCommentRequest, Commentapi.GetCommentResponse

  rpc :DeleteComment, Commentapi.DeleteCommentRequest, Commentapi.DeleteCommentResponse

  rpc :UpdateComment, Commentapi.UpdateCommentRequest, Commentapi.UpdateCommentResponse

  rpc :CreateReply, Commentapi.CreateReplyRequest, Commentapi.CreateReplyResponse

  rpc :GetReply, Commentapi.GetReplyRequest, Commentapi.GetReplyResponse

  rpc :DeleteReply, Commentapi.DeleteReplyRequest, Commentapi.DeleteReplyResponse

  rpc :UpdateReply, Commentapi.UpdateReplyRequest, Commentapi.UpdateReplyResponse
end

defmodule Commentapi.CommentService.Stub do
  @moduledoc false
  use GRPC.Stub, service: Commentapi.CommentService.Service
end
